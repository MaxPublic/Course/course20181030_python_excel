
# coding: utf-8

# # 這是第一堂課
# 主講人:包明勝
# 
# EMail:mixertower@gmail.com
# 
# FB:https://www.facebook.com/MaxPao99/
# 
# 
# ## 寫一個hello python

# In[4]:


print('hello python')


# ## 印出

# In[20]:


a=100
b=200
a,b


# In[21]:


print(a)
print(b)


# ## 變數宣告

# In[13]:


a=100
b='abc'
c=[1,2,3,4,5]
a='abc'
a,b=100,200
a,b


# ## 變數的記憶體位置

# In[14]:


id(a)


# In[16]:


a=a+1
id(a)


# ## 變數可以刪除

# In[18]:


del a
a


# ## 字串使用

# In[22]:


b='abc'
b


# ## 多行字串

# In[25]:


b="""
abc
def
ghi
"""
b


# ## 大小寫

# In[28]:


b='abc'
b=b.upper()
b


# In[32]:


b=b.lower()
b


# ## 變數對調
# 傳統程式語言，要進行變數交換很麻煩
# 
# 但是在Python中，就可以快速地進行

# In[33]:


a=100
b=200
a,b


# In[34]:


a,b=b,a
a,b


# ## 四則運算

# ##### 加法

# In[37]:


a=100
a=a+1
a+=1
a


# ##### 減法

# In[38]:


a-=1
a


# ##### 乘法

# In[39]:


a*=2
a


# ##### 除法

# In[40]:


a/=2
a


# ##### 次方

# In[41]:


a=2
a**3


# ##### 浮點數除法
# 在python3中，已經預設為浮點數除法，不像之前分成兩種除法。
# 
# 所以不用特地去區分

# ##### 餘數

# In[42]:


12 % 5


# ##### 整除法

# In[43]:


10//3


# ## 陣列 List Array

# In[10]:


a=[10,20,30,40,50,60,70,80,90,100]
a


# In[11]:


a[0],a[4]


# In[12]:


a[2:5]


# In[13]:


a[2:]


# In[14]:


a[:7]


# In[16]:


a[-1],a[-2]


# ## 二維陣列

# In[17]:


a=[[1,2,3],[4,5,6],[7,8,9]]
a


# In[18]:


a[1]


# In[19]:


a[1][2]


# ## 字典Dict

# In[21]:


a={'age':18,'name':'Max'}
a


# In[26]:


a['age']


# ## 邏輯判斷式

# In[37]:


a=1
if a==1:
    print('a==1')

if a>0:
    print('a>0')

if a>1:
    print('a>1')
    
if a<1:
    pass
else:
    print(100)
        
if a>0:
    pass

if a>0:
    pass
elif a==1:
    pass
else:
    pass

if a>0 and a<2:
    pass



# ## 迴圈

# In[38]:


a=[10,20,30,40,50]
for i in a:
    print(i)


# In[43]:


a={'age':18,'Name':'Max'}
for i in a:
    print(i)


# In[44]:


for i in a.values():
    print(i)


# In[45]:


for k,v in a.items():
    print(k,v)
    


# In[42]:


for i in range(0,100):
    print(i,end=',')


# ## 字串方法

# In[52]:


st='Get the latest BBC World News: international news, features and analysis from Africa'
st=st.replace(' ','-',3)
st


# In[55]:


a=['10','20','30','40','50']
st='*'
st.join(a)


# In[57]:


st='10*20*30*40*50'
st.split('*',2)

